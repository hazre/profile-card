# Profile-card Template
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

This is a discord-like profile card website

Things you can put up:
- Avatar Paceholder
- Buttons Placeholders
- Links section / Email section
- Infomation area / Bio
- Social links area

Inspired by Discord's Profile tab

You are free to use this template as you like.